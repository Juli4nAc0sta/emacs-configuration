# Emacs Configuration

[![Emacs](https://www.gnu.org/software/emacs/images/emacs.png)](https://www.gnu.org/software/emacs/)

This is my Emacs configuration, there are many on the internet but this one is
mine. Feel free to use it to your liking.

## Programming languages ​​configured

- Rust
- Javascript (VueJS, React Native), HTML & CSS
- Markdown
- Python3

Author: <a href="https://www.julianacosta.co" rel="nofollow noreferrer noopener" target="_blank">
<img width="100" height="100" src="https://i.imgur.com/1VGMY99.png" alt="Julian Acosta">
</a>
