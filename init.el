;;; init.el --- Personal configuration of Emacs

;; Filename: init.el
;; Description: Personal configuration of Emacs
;; Author: Julian Acosta <me@julianacosta.co>
;; Maintainer: Julian Acosta <me@julianacosta.co>
;; Created: 2018-12-02 19:32:08
;; Version: 0.0.2
;; License: GPLv3
;; Last-Updated: 2019-01-29 68:15:26
;;           By: Julian Acosta
;; URL: https://gitlab.com/Juli4nAc0sta/emacs-configuration
;; Keywords: multi-shell, shell
;; Compatibility: GNU Emacs 26.1
;;

;;; Commentary:
;;
;; This is my Emacs configuration, there are many on the internet
;; but this is mine.

;; This configuration is focused to work with the following programming
;; languages, frameworks and tools:
;; Rust, Python3, Javascript (VueJS, React Native), HTML, CSS, Markdown
;;

;;; Change log:
;; 2019/29/01
;;      - Minor changes
;;
;; 2018/12/03
;;      - Organized code and other minor changes
;;      - Change global-highlight-parentheses-mode to show-paren-mode
;;      - Release new version
;;
;; 2018/12/02
;;      First released.
;;

;;; Code:
(require 'package)
(package-initialize)
(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/"))

;;;╔══════════════════════════╦════════════╦══════════════════════════╗
;;;╠══════════════════════════╣Auto install╠══════════════════════════╣
;;;╚══════════════════════════╩════════════╩══════════════════════════╝

(defvar packages-list '(doom-themes
	                flycheck
	                auto-package-update
	                expand-region
	                rust-mode
	                flycheck-rust
	                company
	                aggressive-indent
	                smartparens
	                racer
	                cargo
	                dumb-jump
	                multiple-cursors
	                deadgrep
                        py-autopep8
                        pyvenv
                        yapfify
                        json-mode
                        dotenv-mode
                        wakatime-mode))
(package-refresh-contents)
(dolist (p packages-list)
  (when (not (package-installed-p p))
    (package-install p)
    )
  )

;;;╔════════════════════════════╦═══════╦═════════════════════════════╗
;;;╠════════════════════════════╣Require╠═════════════════════════════╣
;;;╚════════════════════════════╩═══════╩═════════════════════════════╝

(require 'doom-themes)
(require 'expand-region)
(require 'auto-package-update)
(require 'flycheck)
(require 'company)
(require 'rust-mode)

;;;╔═════════════════════════════╦══════╦═════════════════════════════╗
;;;╠═════════════════════════════╣Global╠═════════════════════════════╣
;;;╚═════════════════════════════╩══════╩═════════════════════════════╝
(load-theme 'doom-one t)
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(delete-selection-mode t)
(transient-mark-mode t)
(ido-mode t)
(setq column-number-mode t)
(setq inhibit-startup-message t)
(setq backup-inhibited t)
(setq auto-save-default nil)
(setq make-backup-files nil)
(setq auto-save-list-file-prefix nil)
(setq-default indent-tabs-mode nil)
(setq-default ido-enable-flex-matching t
              ido-use-virtual-buffers t)
(setq-default x-select-enable-clipboard t)
(setq-default show-paren-delay 0)
(setq-default company-tooltip-align-annotations t)

(show-paren-mode t)
(global-flycheck-mode t)
(smartparens-global-mode t)
(global-aggressive-indent-mode t)
(global-company-mode t)
(dumb-jump-mode)
(global-wakatime-mode)
(global-auto-revert-mode t)

;;;╔════════════════════════════╦════════╦════════════════════════════╗
;;;╠════════════════════════════╣Flycheck╠════════════════════════════╣
;;;╚════════════════════════════╩════════╩════════════════════════════╝

(flycheck-add-mode 'javascript-eslint 'mhtml-mode)
(setq-default flycheck-temp-prefix ".flycheck")
(defun my/use-eslint-from-node-modules ()
  "Load eslint from node_modules folder."
  (let* ((root (locate-dominating-file
                (or (buffer-file-name) default-directory)
                "node_modules"))
         (eslint (and root
                      (expand-file-name "node_modules/eslint/bin/eslint.js"
                                        root))))
    (when (and eslint (file-executable-p eslint))
      (setq-local flycheck-javascript-eslint-executable eslint))))
(add-hook 'flycheck-mode-hook #'my/use-eslint-from-node-modules)

;;;╔════════════════════════════╦════════╦════════════════════════════╗
;;;╠════════════════════════════╣WindMove╠════════════════════════════╣
;;;╚════════════════════════════╩════════╩════════════════════════════╝

(global-set-key (kbd "C-c <left>")  'windmove-left)
(global-set-key (kbd "C-c <right>") 'windmove-right)
(global-set-key (kbd "C-c <up>")    'windmove-up)
(global-set-key (kbd "C-c <down>")  'windmove-down)

;;;╔══════════════════════════╦════════════╦══════════════════════════╗
;;;╠══════════════════════════╣ExpandRegion╠══════════════════════════╣
;;;╚══════════════════════════╩════════════╩══════════════════════════╝

(global-set-key (kbd "C-+") 'er/expand-region)
(global-set-key (kbd "C--") 'er/contract-region)

;;;╔══════════════════════════════╦════╦══════════════════════════════╗
;;;╠══════════════════════════════╣Rust╠══════════════════════════════╣
;;;╚══════════════════════════════╩════╩══════════════════════════════╝

(autoload 'rust-mode "rust-mode" nil t)
(setq-default rust-format-on-save t)
(add-hook 'rust-mode-hook #'racer-mode)
(add-hook 'racer-mode-hook #'eldoc-mode)
(add-hook 'racer-mode-hook #'company-mode)
(define-key rust-mode-map (kbd "TAB") #'company-indent-or-complete-common)
(add-hook 'rust-mode-hook 'cargo-minor-mode)

;;;╔═════════════════════════╦══════════════╦═════════════════════════╗
;;;╠═════════════════════════╣WebDevelopment╠═════════════════════════╣
;;;╚═════════════════════════╩══════════════╩═════════════════════════╝

(setq-default mhtml-tag-relative-indent nil)
(setq-default js-indent-level 2)
(setq-default css-indent-offset 2)

;;;╔═════════════════════════════╦══════╦═════════════════════════════╗
;;;╠═════════════════════════════╣Python╠═════════════════════════════╣
;;;╚═════════════════════════════╩══════╩═════════════════════════════╝

(add-hook 'python-mode-hook 'yapf-mode)
(add-hook 'python-mode-hook 'py-autopep8-enable-on-save)

;;;╔════════════════════════════╦════════╦════════════════════════════╗
;;;╠════════════════════════════╣DumbJump╠════════════════════════════╣
;;;╚════════════════════════════╩════════╩════════════════════════════╝

(setq-default dumb-jump-prefer-searcher 'rg)
(setq-default dumb-jump-force-searcher 'rg)

;;;╔═════════════════════════╦═══════════════╦════════════════════════╗
;;;╠═════════════════════════╣MultipleCursors╠════════════════════════╣
;;;╚═════════════════════════╩═══════════════╩════════════════════════╝

(global-set-key (kbd "C-c l") 'mc/edit-lines)
(global-set-key (kbd "C-c n") 'mc/mark-next-like-this)
(global-set-key (kbd "C-c p") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c a") 'mc/mark-all-like-this)

;;;╔════════════════════════════╦════════╦════════════════════════════╗
;;;╠════════════════════════════╣DeadGrep╠════════════════════════════╣
;;;╚════════════════════════════╩════════╩════════════════════════════╝

(global-set-key (kbd "<f5>") #'deadgrep)

;;;╔══════════════════════════╦═════════════╦═════════════════════════╗
;;;╠══════════════════════════╣AutoModeAlist╠═════════════════════════╣
;;;╚══════════════════════════╩═════════════╩═════════════════════════╝

(add-to-list 'auto-mode-alist '("\\.rs\\'" . rust-mode))
(add-to-list 'auto-mode-alist '("\\.html\\'" . html-mode))
(add-to-list 'auto-mode-alist '("\\.js\\'" . js-mode))
(add-to-list 'auto-mode-alist '("\\.css\\'" . css-mode))
(add-to-list 'auto-mode-alist '("\\.vue\\'" . mhtml-mode))
(add-to-list 'auto-mode-alist '("\\.gradle\\'" . groovy-mode))
(add-to-list 'auto-mode-alist '("\\.json\\'" . json-mode))

;;; init.el ends here

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(dockerfile-mode yaml-mode wakatime-mode dotenv-mode web-mode php-mode yapfify py-autopep8 json-mode expand-region auto-package-update smartparens racer pyvenv multiple-cursors magit flycheck-rust dumb-jump doom-themes deadgrep company cargo aggressive-indent)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
;; Local Variables:
;; coding: utf-8
;; indent-tabs-mode: nil
;; End:

